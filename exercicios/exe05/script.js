var lista = [];

function init() {
    document.getElementById('enviar').addEventListener('click', receberItens);
}

function receberItens() {
    var somatorio = 0;

    lista.push({
        nome: document.getElementById('nome').value,
        preco: document.getElementById('preco').value
    });

    for (var l of lista) {
        somatorio += parseFloat(l.preco);
    }

    document.getElementById('total').innerHTML = somatorio;

    if (somatorio > 49){
        document.getElementById('total').style.color = 'red';
    }

    var novo = document.createElement("p");

    novo.innerHTML = document.getElementById('nome').value + " - " +  document.getElementById('preco').value;
    document.getElementById("p").appendChild(novo);

}

init();

