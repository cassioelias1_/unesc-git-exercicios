var express = require('express');
var router = express.Router();
const fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  let produtos = null;
  try {
    produtos = fs.readFileSync('produtos.json', 'utf8');
  } catch (error) {}

  if(produtos != null && produtos.length > 0){
    produtos = JSON.parse(produtos);
  } else {
    produtos = [];
  }

  res.render('index', { title: 'Express', produtos: produtos });
});

router.get('/saveproduto', function(req, res, next) {
  let produtos = null;
  try {
    produtos = fs.readFileSync('produtos.json', 'utf8');
  } catch (error) {}

  if(produtos != null && produtos.length > 0){
    produtos = JSON.parse(produtos);
  } else {
    produtos = [];
  }

  produtos = [...produtos, req.query.produto]
  .filter(f => f != null && f.length > 0);
  
  fs.writeFileSync('produtos.json', JSON.stringify(produtos), {encoding: 'utf-8', flag: 'w'});
  res.render('index', { title: 'Express', produtos: produtos });
});

module.exports = router;
