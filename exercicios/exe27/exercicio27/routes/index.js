var express = require('express');
var router = express.Router();
const fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/save', function(req, res, next) {
  let palavras = null;
  try {
    palavras = fs.readFileSync('frases.json', 'utf8');
  } catch (error) {}

  if(palavras != null && palavras.length > 0){
    palavras = JSON.parse(palavras);
  } else {
    palavras = [];//Obrigatório para não da ruim no new Set
  }

  //Criar um set que não permite itens repetidos com as palavras existentes e a nova frase informada
  let naoRepetidas = [...new Set([...palavras, req.body.frase])]
  .filter(f => f != null && f.length > 0);
  
  fs.writeFileSync('frases.json', JSON.stringify(naoRepetidas), {encoding: 'utf-8', flag: 'w'});
  res.render('index', { title: 'Express' });
});


module.exports = router;
