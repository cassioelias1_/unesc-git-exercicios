class GeradorLista extends Gerador {
    constructor(){
        super();
        this.lista = [];
    }

    addLista(value){
        this.lista.push(value);
    }

    getLista(){
        return this.lista;
    }
}

