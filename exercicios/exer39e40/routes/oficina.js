var express = require('express');
var router = express.Router();
var fs = require('fs');

router.get('/', function(req, res, next) {
    var oficina = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).oficina;
    
    res.json(oficina);
  });
  
  router.delete('/:_id', function (req, res) {
    var dados = JSON.parse(getAllFile());
    var oficinas = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).oficina;
    
    var index = findById(req.params._id, oficinas);
  
    var oficina = 'Oficina não encontrada!';
    if(index != -1){
      oficina = oficinas.splice(index, 1);
    }

    dados.oficina = oficinas;
    
    saveInFile(dados);
  
    res.json(oficina);
  });
  
  /*
* Adicione estes campos no body
*  nome,
*  professor,
*  local (somente ID),
*  participantes (Somentes ids separados por virgula)
*/
  router.post('/', function(req, res) {
    var dados = JSON.parse(getAllFile());
    var oficinas = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).oficina;
    var newOficina = req.body;
    var ultId = getUltId(oficinas);
    newOficina._id = ultId + 1;
  
    newOficina.participantes = newOficina.participantes.split(',');

    oficinas.push(newOficina);
    dados.oficina = oficinas;
    
    saveInFile(dados);
    
    res.json(newOficina);
  });
  
   /*
* Adicione estes campos no body
*  nome,
*  professor,
*  local (somente ID),
*  participantes (Somentes ids separados por virgula)
* Adicione no params o id do local que deseja editar
*/ 
  router.put('/:_id', function(req, res) {
    var dados = JSON.parse(getAllFile());
    var oficinas = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).oficina;
    
    var index = findById(req.params._id, oficinas);
  
    if(index != -1){
      oficinas[index] = req.body;
      oficinas[index].participantes = oficinas[index].participantes.split(',');
    }
    

    dados.oficina = oficinas;

    saveInFile(dados);

    res.send('Oficina atualizada com sucesso');
  });

/*
* Adicione estes campos no body
*  _idParticipante,
*  inserir (true ou false, mandar como String mesmo)
* Adicione no params o id da oficina que deseja adicionar um participante
*/ 
  router.put('/check-in/:_id', function(req, res){   
    var dados = JSON.parse(getAllFile()); 
    var oficinas = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).oficina;
    var locais = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).local;    
    var index = findById(req.params._id, oficinas);
    
    var idParticipante = parseInt(req.body._idParticipante);//id do participante a ser removido ou adicionado

    var oficina = oficinas[index];
    
    var indexLocal = findById(oficina.local, locais);  
    var local = locais[indexLocal];

    var inserir = req.body.inserir == 'true' ? true : false;
  
    if(inserir){//flag que deve ser encaminhada para decidir se será inserido ou removido.
      
      if (oficina.participantes.length >= local.capacidade){
        var erro = {
          'msg': 'A capacidade maxima foi atingida',
          'codigo': 400
        };
    
        res.json(erro);
        return;
      }

      oficina.participantes.push(idParticipante);

      oficinas[index] = oficina;

      dados.oficina = oficinas;

      saveInFile(dados);
      
      res.send('Participante adicionado com sucesso!');
    } else {//remover
      if(oficina.participantes.length < 1){
        var erro = {
          'msg': 'Todos participantes já foram removidos',
          'codigo': 400
        };
    
        res.json(JSON.parse(erro));
      }
      var indexParticipante = oficina.participantes.findIndex(function (item) {
        if(item == idParticipante){
          return true;
        }
        return false;
      });

      var participanteRemovido = 'Participante não encontrado na oficina';
      if(indexParticipante != -1){
        participanteRemovido = oficina.participantes.splice(indexParticipante, 1);
      }
      res.json(participanteRemovido);
    }
  });


function getAllFile(){
  var file = null
  try{
    file = fs.readFileSync('dados.json', 'utf-8');
  }catch(error){}

  return file;
}

function saveInFile(dados){
  fs.writeFileSync('dados.json', JSON.stringify(dados), {encoding: 'utf-8', flag: 'w'});
}

function getUltId(lista) {
  var maior = 0;

  lista.map(function(item){
    maior = Math.max(item._id);
  });

  return maior;
}

function findById(id, lista) {
  return lista.findIndex(function (item) {
    if(item._id == id){
      return true;
    }
    return false;
  });
}


module.exports = router;