var express = require('express');
var router = express.Router();
var fs = require('fs');

router.get('/', function(req, res, next) {
  var locais = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).local;
  
  res.json(locais);
});
  

router.delete('/:_id', function (req, res) {
  var dados = JSON.parse(getAllFile());
  var locais = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).local;
  var index = findById(req.params._id, locais);

  var local = 'Local não encontrado!';
  if(index != -1){
    local = locais.splice(index, 1);
  }
  dados.local = locais;
  
  saveInFile(dados);

  res.json(local);
});

/*
* Adicione estes campos no body
*  nome,
*  capacidade
*
*/
router.post('/', function(req, res) {
  var dados = JSON.parse(getAllFile());
  var locais = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).local;
  var newLocal = req.body;
  newLocal.capacidade = parseInt(newLocal.capacidade);
  var ultId = getUltId(locais);
  newLocal._id = ultId + 1;
  locais.push(newLocal);
  
  dados.local = locais;

  saveInFile(dados);

  res.json(newLocal);
});

/*
* Adicione estes campos no body
*  nome,
*  capacidade
* Adicione no params o id do local que deseja editar
*/
router.put('/:_id', function(req, res) {
  var dados = JSON.parse(getAllFile());
  var locais = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).local;
  var index = findById(req.params._id, locais);
  
  if(index != -1){
    locais[index].nome = req.body.nome;
    locais[index].capacidade = parseInt(req.body.capacidade);
  }

  dados.local = locais;

  saveInFile(dados);
  
  res.send('Local atualizado com sucesso');
});

function getAllFile(){
  var file = null
  try{
    file = fs.readFileSync('dados.json', 'utf-8');
  }catch(error){}

  return file;
}

function saveInFile(dados){
  fs.writeFileSync('dados.json', JSON.stringify(dados), {encoding: 'utf-8', flag: 'w'});
}

function getUltId(lista) {
  var maior = 0;

  lista.map(function(item){
    maior = Math.max(item._id);
  });

  return maior;
}

function findById(id, lista) {
  return lista.findIndex(function (item) {
    if(item._id == id){
      return true;
    }
    return false;
  });
}


module.exports = router;