var express = require('express');
var router = express.Router();
var fs = require('fs');

router.get('/', function(req, res, next) {
  var usuarios = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).usuarios;
  
  res.json(usuarios);
});

router.delete('/:_id', function (req, res) {
  var dados = JSON.parse(getAllFile());
  var usuarios = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).usuarios;
  var index = findById(req.params._id, usuarios);

  var usuario = 'Usuário não encontrado!';
  if(index != -1){
    usuario = usuarios.splice(index, 1);
  }
  dados.usuarios = usuarios;
  
  saveInFile(dados);

  res.json(usuario);
});

router.post('/', function(req, res) {
  var dados = JSON.parse(getAllFile());
  var usuarios = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).usuarios;
  var newUsuario = req.body;
  var ultId = getUltId(usuarios);
  newUsuario._id = ultId + 1;
  usuarios.push(newUsuario);

  dados.usuarios = usuarios;
  saveInFile(dados);
  
  res.json(newUsuario);
});


router.put('/:_id', function(req, res) {
  var dados = JSON.parse(getAllFile());
  var usuarios = JSON.parse(fs.readFileSync('dados.json', 'utf-8')).usuarios;
  var index = findById(req.params._id, usuarios);

  if(index != -1){
    usuarios[index].nome = req.body.nome;
  }
  
  dados.usuarios = usuarios;
  saveInFile(dados);
  
  res.send('Usuario atualizado com sucesso');
});

function getAllFile(){
  var file = null
  try{
    file = fs.readFileSync('dados.json', 'utf-8');
  }catch(error){}

  return file;
}

function saveInFile(dados){
  fs.writeFileSync('dados.json', JSON.stringify(dados), {encoding: 'utf-8', flag: 'w'});
}

function getUltId(lista) {
  var maior = 0;

  lista.map(function(item){
    maior = Math.max(item._id);
  });

  return maior;
}

function findById(id, lista) {
  return lista.findIndex(function (item) {
    if(item._id == id){
      return true;
    }
    return false;
  });
}


module.exports = router;